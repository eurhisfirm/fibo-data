# FIBO-Data

_Quads from the [Financial Industry Business Ontology (FIBO)](https://spec.edmcouncil.org/fibo/) converted to JSON using [FIBO-Wikibase](https://gitlab.huma-num.fr/eurhisfirm/fibo-wikibase)_
